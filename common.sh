echomsg() {
	echo -e "\e[44m$1\e[0m"
}

list_lang() {
  local lang=$1
  for name in $(ls $lang) ; do
    if [[ "$name" == 'node_modules' ]]; then
      continue
    fi
    local dir=$(pwd)/$lang/$name
    if [[ -d $dir ]]; then
      echo $dir
    fi
  done
}

list_loop() {
  local lang=$1
  shift
  local cmd=$*
  local cwd=$(pwd)
  for dir in $(list_lang $lang) ; do
    echomsg "cd $dir"
    cd $dir
    echomsg "run $cmd"
    $cmd
  done
  cd $cwd
}

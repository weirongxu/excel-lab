# frozen_string_literal: true

require('rubyXL')

workbook = RubyXL::Parser.parse('../../test-excels/calculate.xlsx')
sheet = workbook[0]
sheet.add_cell(22, 1, 100)
sheet.add_cell(32, 1, 100)
workbook.write('./calculate.xlsx')

workbook = RubyXL::Parser.parse('../../test-excels/chart.xlsx')
sheet = workbook[0]
sheet.add_cell(22, 1, 100)
sheet.add_cell(32, 1, 100)
workbook.write('./chart.xlsx')

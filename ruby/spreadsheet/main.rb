# frozen_string_literal: true

require('spreadsheet')

book = Spreadsheet.open('../../test-excels/calculate.xls')
sheet = book.worksheet(0)
sheet[22, 1] = 100
sheet[32, 1] = 100
book.write('./test-calculate.xls')

book = Spreadsheet.open('../../test-excels/chart.xls')
sheet = book.worksheet(0)
sheet[22, 1] = 100
sheet[32, 1] = 100
book.write('./test-chart.xls')

# frozen_string_literal: true

require('roo')

# Can only read

xlsx = Roo::Spreadsheet.open('../../test-excels/calculate.xlsx')
sheet = xlsx.sheet(0)
sheet.set(23, 'B', 100)
sheet.set(33, 'B', 100)
pp xlsx.to_xml

xlsx = Roo::Spreadsheet.open('../../test-excels/chart.xlsx')
sheet = xlsx.sheet(0)
sheet.set(23, 'B', 100)
sheet.set(33, 'B', 100)
pp xlsx.to_xml

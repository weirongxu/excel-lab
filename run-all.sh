#!/usr/bin/env bash

. common.sh

list_loop ruby rm -f *.{xlsx,xls}
list_loop ruby bundle
list_loop ruby bundle exec ruby main.rb

cd javascript
yarn
cd ..
list_loop javascript rm -f *.{xlsx,xls}
list_loop javascript node index.js

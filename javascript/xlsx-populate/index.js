const XlsxPopulate = require('xlsx-populate');

XlsxPopulate.fromFileAsync('../../test-excels/calculate.xlsx').then(
  (workbook) => {
    workbook.sheet('Sheet1').cell('B23').value(100);
    workbook.sheet('Sheet1').cell('B33').value(100);

    return workbook.toFileAsync('./calculate.xlsx');
  }
);

XlsxPopulate.fromFileAsync('../../test-excels/chart.xlsx').then((workbook) => {
  workbook.sheet('Sheet1').cell('B23').value(100);
  workbook.sheet('Sheet1').cell('B33').value(100);

  return workbook.toFileAsync('./chart.xlsx');
});

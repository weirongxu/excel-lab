const XLSX = require('xlsx');
var workbook;

// time, color format will be invalid
workbook = XLSX.readFile('../../test-excels/calculate.xlsx');
XLSX.writeFile(workbook, 'calculate.xlsx');

// chart will be invalid
workbook = XLSX.readFile('../../test-excels/chart.xlsx');
XLSX.writeFile(workbook, 'chart.xlsx');

const Excel = require('exceljs');

var workbook;
workbook = new Excel.Workbook();
workbook.xlsx
  .readFile('../../test-excels/calculate.xlsx')
  .then(function (workbook) {
    workbook.xlsx.writeFile('calculate.xlsx').then(function () {});
  });

workbook = new Excel.Workbook();
workbook.xlsx
  .readFile('../../test-excels/chart.xlsx')
  .then(function (workbook) {
    // No support chart
    workbook.xlsx.writeFile('chart.xlsx').then(function () {});
  });
